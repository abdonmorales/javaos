// kernel.h
#pragma once

// For now, this is empty, but as your kernel grows, 
// you can add global kernel functions or variables here.