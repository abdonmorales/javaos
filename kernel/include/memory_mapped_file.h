#pragma once

void* mmap_file(const char* filepath);
void munmap_file(void* address);