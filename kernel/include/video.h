// video.h
#pragma once

extern "C" {
    void init_video();
    void print(const char* str);
}
