#pragma once

void *kmalloc(size_t size);
void kfree(void *p);