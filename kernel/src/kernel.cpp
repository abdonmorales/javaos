// kernel.cpp

extern "C" void init_video();
extern "C" void print(const char* str);

class Kernel {
public:
    void start() {
        init_video();
        print("Hello from JavaOS Kernel");
    }
};

extern "C" void kernel_main() {
    Kernel kernel;
    kernel.start();
}
