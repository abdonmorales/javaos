extern "C" {

// VGA text mode starts at memory address 0xB8000
volatile char* video_memory = (volatile char*) 0xB8000;

// Screen dimensions for VGA text mode
const int VGA_WIDTH = 80;
const int VGA_HEIGHT = 25;

// Current cursor position
int cursor_x = 0;
int cursor_y = 0;

// Function to write a byte to an I/O port (defined in inline assembly)
inline void outb(unsigned short port, unsigned char value) {
    asm volatile ("outb %1, %0" : : "dN" (port), "a" (value));
}

// Function to update the hardware cursor position
void move_cursor() {
    unsigned short position = cursor_y * VGA_WIDTH + cursor_x;
    // Send command to VGA index register
    outb(0x3D4, 0x0F);
    outb(0x3D5, (unsigned char)(position & 0xFF));
    outb(0x3D4, 0x0E);
    outb(0x3D5, (unsigned char)((position >> 8) & 0xFF));
}

void init_video() {
    // Clear the screen
    for (int y = 0; y < VGA_HEIGHT; y++) {
        for (int x = 0; x < VGA_WIDTH; x++) {
            video_memory[(y * VGA_WIDTH + x) * 2] = ' ';
            video_memory[(y * VGA_WIDTH + x) * 2 + 1] = 0x07;  // Light gray on black background
        }
    }
    cursor_x = 0;
    cursor_y = 0;
    move_cursor();
}

void print(const char* str) {
    int i = 0;
    while (str[i]) {
        switch (str[i]) {
            case '\n':  // Newline character
                cursor_x = 0;
                cursor_y++;
                break;
            default:
                video_memory[(cursor_y * VGA_WIDTH + cursor_x) * 2] = str[i];
                video_memory[(cursor_y * VGA_WIDTH + cursor_x) * 2 + 1] = 0x07;  // Light gray on black background
                cursor_x++;
                if (cursor_x >= VGA_WIDTH) {
                    cursor_x = 0;
                    cursor_y++;
                }
                break;
        }
        if (cursor_y >= VGA_HEIGHT) {
            // Scroll the screen (for simplicity, just clear it for now)
            init_video();
        }
        i++;
    }
    move_cursor();
}

}