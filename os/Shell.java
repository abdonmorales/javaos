package os;
import java.util.Scanner;
import java.nio.file.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;
import java.util.Properties;
import java.util.stream.Collectors;

import apps.calculator.Calculator;
import apps.filebrowser.FileBrowser;
import apps.texteditor.TextEditor;

import java.applet.Applet;
import java.awt.Graphics;
import java.awt.Color;
import java.awt.Frame;

public class Shell {

    private static final String JAVA_BINARY_PATH = "jdk/bin/java";
    private static final String LIBS_PATH = "libs/*";  // The * denotes all JAR files in the directory
    private static final String JAVAC_BINARY_PATH = "jdk/bin/javac";
    private static final String JAVAFX_LIB_PATH = "javafx/lib/*";
    private static final String JAVAFX_MODULES = "--module-path javafx/lib --add-modules javafx.controls,javafx.fxml";
    private static final String OS_VERSION = "JavaOS 1.0";  // Adjust this as per your OS version
    private Path currentDirectory = Paths.get(".").toAbsolutePath().normalize();

    public static void main(String[] args) {
        System.setProperty("JAVA_HOME", "../jdk");
        new Shell().start();
    }

    public void start() {
        try (Scanner scanner = new Scanner(System.in)) {
		String command;

		while (true) {
		    System.out.print(currentDirectory + " > ");
		    command = scanner.nextLine();

		    String[] parts = command.split(" ", 2);
		    String cmd = parts[0];
		    String argument = parts.length > 1 ? parts[1] : "";

		    switch (cmd) {
		        case "jhelp":
		            System.out.println("Available commands: jhelp, jexit, jdate, jecho, jcd, jpwd, jrename, jcat, jls, jclear, jrun, jversion, japplet, mkdir, jrun, jcompile, jedit");
		            break;
		        case "jexit":
		            System.out.println("Exiting JavaOS...");
		            System.exit(0);
		            break;
		        case "jdate":
		            System.out.println(new java.util.Date());
		            break;
		        case "jecho":
		            System.out.println(argument);
		            break;
		        case "jcd":
		            changeDirectory(argument);
		            break;
		        case "jpwd":
		            System.out.println(currentDirectory);
		            break;
		        case "jrename":
		            String[] renameArgs = argument.split(" ");
		            if (renameArgs.length == 2) {
		                rename(renameArgs[0], renameArgs[1]);
		            } else {
		                System.out.println("Usage: jrename [oldName] [newName]");
		            }
		            break;
		        case "jcat":
		            jcat(argument);
		            break;
		        case "jls":
		            jls();
		            break;
		        case "jclear":
		            clearConsole();
		            break;
		        case "jrun":
		            jrun(argument);
		            break;
		        case "jversion":
		            jversion();
		            break;
                case "japplet":
                    japplet(argument);
                    break;
                case "mkdir":
                    mkdir(argument);
                    break;
                case "jcompile":
                    jcompile(argument);
                    break;
                case "jedit":
                    runTextEditor();
                    break;
                case "filebrowser":
                    runFileBrowser();
                    break;
                case "calculator":
                    runCalculator();
                    break;
		        default:
		            System.out.println("Unknown command: " + cmd);
		    }
		}
	}
    }
    private void changeDirectory(String dir) {
        Path newPath = currentDirectory.resolve(dir).normalize();
        if (Files.exists(newPath) && Files.isDirectory(newPath)) {
            currentDirectory = newPath;
        } else {
            System.out.println("Directory not found: " + dir);
        }
    }
    private void runTextEditor() {
    // Assuming TextEditor has a main method to launch the application
        TextEditor.main(new String[]{});
    }
    private void rename(String oldName, String newName) {
        Path oldPath = currentDirectory.resolve(oldName);
        Path newPath = currentDirectory.resolve(newName);
        try {
            Files.move(oldPath, newPath);
            System.out.println("Renamed " + oldName + " to " + newName);
        } catch (IOException e) {
            System.out.println("Error renaming: " + e.getMessage());
        }
    }

    private void jcat(String fileName) {
        Path filePath = currentDirectory.resolve(fileName);
        try {
            List<String> lines = Files.readAllLines(filePath);
            for (String line : lines) {
                System.out.println(line);
            }
        } catch (IOException e) {
            System.out.println("Error reading file: " + e.getMessage());
        }
    }

    private void jls() {
        try {
            List<Path> paths = Files.list(currentDirectory).collect(Collectors.toList());
            for (Path path : paths) {
                System.out.println(path.getFileName());
            }
        } catch (IOException e) {
            System.out.println("Error listing directory: " + e.getMessage());
        }
    }

    private void clearConsole() {
        for (int i = 0; i < 100; i++) {
            System.out.println();
        }
    }

    private void jrun(String className) {
        try {
            ProcessBuilder processBuilder = new ProcessBuilder(JAVA_BINARY_PATH, "-cp", LIBS_PATH + ":" + JAVAFX_LIB_PATH, JAVAFX_MODULES, className);
            processBuilder.directory(currentDirectory.toFile());
            Process process = processBuilder.start();
            Scanner processOutput = new Scanner(process.getInputStream());
            while (processOutput.hasNextLine()) {
                System.out.println(processOutput.nextLine());
            }
            processOutput.close();
        } catch (Exception e) {
            System.out.println("Error executing Java application: " + e.getMessage());
        }
    }

    private void japplet(String appletClassName) {
        try {
            Class<?> appletClass = Class.forName(appletClassName);
            Applet appletInstance = (Applet) appletClass.getDeclaredConstructor().newInstance();

            Frame frame = new Frame("JavaOS Applet Viewer");
            frame.setSize(800, 600);
            frame.add(appletInstance);
            frame.setVisible(true);

            appletInstance.init();
            appletInstance.start();

        } catch (Exception e) {
            System.out.println("Error running applet: " + e.getMessage());
        }
    }
    private void mkdir(String dirName) {
        Path newDir = currentDirectory.resolve(dirName);
        try {
            Files.createDirectory(newDir);
            System.out.println("Directory created: " + dirName);
        } catch (IOException e) {
            System.out.println("Error creating directory: " + e.getMessage());
        }
    }
    private void jversion() {
    System.out.println("Operating System: " + OS_VERSION);
    try {
        Process javaVersionProcess = new ProcessBuilder(JAVA_BINARY_PATH, "-version").start();
        printProcessOutput(javaVersionProcess);

        // Fetching JavaFX version from javafx.properties
        Path javafxPropertiesPath = Paths.get("javafx/lib/javafx.properties");
        if (Files.exists(javafxPropertiesPath)) {
            Properties javafxProperties = new Properties();
            try (InputStream inputStream = Files.newInputStream(javafxPropertiesPath)) {
                javafxProperties.load(inputStream);
            }
            String javafxVersion = javafxProperties.getProperty("javafx.version");
            System.out.println("JavaFX version: " + (javafxVersion != null ? javafxVersion : "Not available"));
            } else {
                System.out.println("JavaFX version: Not available");
            }
        } catch (Exception e) {
            System.out.println("Error fetching versions: " + e.getMessage());
        }
    }

    private void printProcessOutput(Process process) throws IOException {
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(process.getErrorStream()))) {
            String line;
            while ((line = reader.readLine()) != null) {
                System.out.println(line);
            }
        }
    }
    private void jcompile(String sourceFile) {
        try {
            ProcessBuilder processBuilder = new ProcessBuilder(JAVAC_BINARY_PATH, "-cp", LIBS_PATH + ":" + JAVAFX_LIB_PATH, JAVAFX_MODULES, sourceFile);
            processBuilder.directory(currentDirectory.toFile());
            Process process = processBuilder.start();
            Scanner processOutput = new Scanner(process.getInputStream());
            while (processOutput.hasNextLine()) {
                System.out.println(processOutput.nextLine());
            }
            processOutput.close();
        } catch (Exception e) {
            System.out.println("Error compiling Java file: " + e.getMessage());
        }
    }
    private void runFileBrowser() {
    // Run FileBrowser in a new thread to allow shell to continue accepting commands
        new Thread(() -> {
            FileBrowser.main(new String[]{});
        }).start();
    }
    private void runCalculator() {
    // Run Calculator in a new thread to allow shell to continue accepting commands
        new Thread(() -> {
            Calculator.main(new String[]{});
        }).start();
    }
}