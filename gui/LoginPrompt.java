package gui;
import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class LoginPrompt extends Application {

    private boolean authenticated = false;

    @Override
    public void start(Stage primaryStage) {
        primaryStage.setTitle("JavaOS Login");

        GridPane grid = new GridPane();
        grid.setAlignment(Pos.CENTER);
        grid.setVgap(10);
        grid.setHgap(10);

        Label userLabel = new Label("Username:");
        TextField userField = new TextField();
        Label passLabel = new Label("Password:");
        PasswordField passField = new PasswordField();

        Button loginButton = new Button("Login");
        loginButton.setOnAction(e -> {
            // For demonstration, we assume a hardcoded username and password.
            // In a real-world scenario, you'd check against a database or other secure storage.
            if ("admin".equals(userField.getText()) && "password".equals(passField.getText())) {
                authenticated = true;
                primaryStage.close();
            } else {
                showAlert("Login Failed", "Incorrect username or password.");
            }
        });

        grid.add(userLabel, 0, 0);
        grid.add(userField, 1, 0);
        grid.add(passLabel, 0, 1);
        grid.add(passField, 1, 1);
        grid.add(loginButton, 1, 2);

        Scene scene = new Scene(grid, 300, 200);
        primaryStage.setScene(scene);
        primaryStage.showAndWait();

        if (authenticated) {
            // Launch the main JavaOS GUI or any other logic post-authentication
            JavaOSGUI.main(new String[]{});
        }
    }

    private void showAlert(String title, String message) {
        Stage alertStage = new Stage();
        alertStage.initModality(Modality.APPLICATION_MODAL);

        VBox vbox = new VBox(new Label(message));
        vbox.setAlignment(Pos.CENTER);
        vbox.setPadding(new Insets(10));

        alertStage.setScene(new Scene(vbox));
        alertStage.setTitle(title);
        alertStage.showAndWait();
    }

    public static void main(String[] args) {
        launch(args);
    }
}