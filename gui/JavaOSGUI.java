package gui;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.MenuButton;
import javafx.scene.control.MenuItem;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

public class JavaOSGUI extends Application {

    @Override
    public void start(Stage primaryStage) {
        BorderPane root = new BorderPane();

        // Taskbar
        HBox taskbar = new HBox();
        taskbar.setStyle("-fx-background-color: #2e2e2e; -fx-padding: 5px;");

        // Start Menu
        MenuButton startMenu = new MenuButton("Start");
        startMenu.getItems().addAll(
                new MenuItem("Text Editor"),
                new MenuItem("File Browser"),
                new MenuItem("Calculator")
        );
        taskbar.getChildren().add(startMenu);

        root.setBottom(taskbar);

        // Set up the main scene
        Scene scene = new Scene(root, 800, 600);
        primaryStage.setTitle("JavaOS");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
