package gui;
import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import data.UserDatabase;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import java.sql.SQLException;

public class RegistrationPrompt extends Application {

    private UserDatabase userDatabase = new UserDatabase();

    @Override
    public void start(Stage primaryStage) {
        primaryStage.setTitle("JavaOS Registration");

        GridPane grid = new GridPane();
        grid.setAlignment(Pos.CENTER);
        grid.setVgap(10);
        grid.setHgap(10);

        Label userLabel = new Label("Username:");
        TextField userField = new TextField();
        Label passLabel = new Label("Password:");
        PasswordField passField = new PasswordField();
        Label emailLabel = new Label("Email:");
        TextField emailField = new TextField();

        Button registerButton = new Button("Register");
        registerButton.setOnAction(e -> {
            try {
                userDatabase.registerUser(userField.getText(), passField.getText(), emailField.getText());
                showAlert("Registration Successful", "You can now login with your credentials.");
                primaryStage.close();
            } catch (SQLException ex) {
                showAlert("Registration Failed", "Error occurred while registering: " + ex.getMessage());
            }
        });

        grid.add(userLabel, 0, 0);
        grid.add(userField, 1, 0);
        grid.add(passLabel, 0, 1);
        grid.add(passField, 1, 1);
        grid.add(emailLabel, 0, 2);
        grid.add(emailField, 1, 2);
        grid.add(registerButton, 1, 3);

        Scene scene = new Scene(grid, 300, 250);
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    private void showAlert(String title, String message) {
        Stage alertStage = new Stage();
        VBox vbox = new VBox(new Label(message));
        vbox.setAlignment(Pos.CENTER);
        vbox.setPadding(new javafx.geometry.Insets(10));
        alertStage.setScene(new Scene(vbox));
        alertStage.setTitle(title);
        alertStage.showAndWait();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
