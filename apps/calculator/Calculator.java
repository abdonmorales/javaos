package apps.calculator;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class Calculator {
    private JFrame frame;
    private JTextField display;
    private String currentOperator;
    private double firstOperand;
    private boolean startNumber = true;

    public Calculator() {
        frame = new JFrame("JavaOS Calculator");
        display = new JTextField("0");
        display.setEditable(false);

        JPanel panel = new JPanel();
        panel.setLayout(new GridLayout(4, 4));

        String[] buttons = {"7", "8", "9", "/", "4", "5", "6", "*", "1", "2", "3", "-", "0", ".", "=", "+"};
        for (String text : buttons) {
            JButton button = new JButton(text);
            button.addActionListener(e -> buttonClicked(e.getActionCommand()));
            panel.add(button);
        }

        frame.setLayout(new BorderLayout());
        frame.add(display, BorderLayout.NORTH);
        frame.add(panel);
        frame.pack();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    private void buttonClicked(String text) {
        if ("0123456789.".contains(text)) {
            if (startNumber) {
                display.setText(text);
                startNumber = false;
            } else {
                display.setText(display.getText() + text);
            }
        } else if ("+-*/".contains(text)) {
            currentOperator = text;
            firstOperand = Double.parseDouble(display.getText());
            startNumber = true;
        } else if ("=".equals(text)) {
            double secondOperand = Double.parseDouble(display.getText());
            switch (currentOperator) {
                case "+":
                    display.setText(String.valueOf(firstOperand + secondOperand));
                    break;
                case "-":
                    display.setText(String.valueOf(firstOperand - secondOperand));
                    break;
                case "*":
                    display.setText(String.valueOf(firstOperand * secondOperand));
                    break;
                case "/":
                    display.setText(String.valueOf(firstOperand / secondOperand));
                    break;
            }
            startNumber = true;
        }
    }

    public void show() {
        frame.setVisible(true);
    }

    public static void main(String[] args) {
        new Calculator().show();
    }
}