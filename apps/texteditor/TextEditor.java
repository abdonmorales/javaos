package apps.texteditor;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.IOException;
import java.nio.file.*;
import java.nio.charset.*;

public class TextEditor {
    private JFrame frame;
    private JTextArea textArea;
    private JFileChooser fileChooser;

    public TextEditor() {
        frame = new JFrame("JavaOS Text Editor");
        textArea = new JTextArea(20, 60);
        fileChooser = new JFileChooser();

        frame.setLayout(new BorderLayout());
        frame.add(new JScrollPane(textArea), BorderLayout.CENTER);
        frame.add(createToolbar(), BorderLayout.NORTH);
        frame.pack();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    private JToolBar createToolbar() {
        JToolBar toolbar = new JToolBar();
        JButton openButton = new JButton("Open");
        JButton saveButton = new JButton("Save");
        toolbar.add(openButton);
        toolbar.add(saveButton);

        openButton.addActionListener(e -> openFile());
        saveButton.addActionListener(e -> saveFile());

        return toolbar;
    }

    private void openFile() {
        int returnValue = fileChooser.showOpenDialog(frame);
        if (returnValue == JFileChooser.APPROVE_OPTION) {
            Path file = fileChooser.getSelectedFile().toPath();
            try {
                String content = new String(Files.readAllBytes(file), StandardCharsets.UTF_8);
                textArea.setText(content);
            } catch (IOException e) {
                JOptionPane.showMessageDialog(frame, "Error reading file: " + e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    private void saveFile() {
        int returnValue = fileChooser.showSaveDialog(frame);
        if (returnValue == JFileChooser.APPROVE_OPTION) {
            Path file = fileChooser.getSelectedFile().toPath();
            try {
                Files.write(file, textArea.getText().getBytes(StandardCharsets.UTF_8));
            } catch (IOException e) {
                JOptionPane.showMessageDialog(frame, "Error saving file: " + e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    public void show() {
        frame.setVisible(true);
    }

    public static void main(String[] args) {
        new TextEditor().show();
    }
}