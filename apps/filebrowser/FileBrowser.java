package apps.filebrowser;
import javax.swing.*;
import javax.swing.tree.*;
import java.io.File;

public class FileBrowser {
    private JFrame frame;
    private JTree tree;
    private DefaultTreeModel treeModel;

    public FileBrowser() {
        frame = new JFrame("JavaOS File Browser");
        DefaultMutableTreeNode root = new DefaultMutableTreeNode("/");
        treeModel = new DefaultTreeModel(root);
        tree = new JTree(treeModel);
        populateFileSystem(root, new File("/"));

        frame.add(new JScrollPane(tree));
        frame.setSize(400, 300);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    private void populateFileSystem(DefaultMutableTreeNode node, File file) {
        for (File child : file.listFiles()) {
            DefaultMutableTreeNode childNode = new DefaultMutableTreeNode(child.getName());
            node.add(childNode);
            if (child.isDirectory()) {
                populateFileSystem(childNode, child);
            }
        }
    }

    public void show() {
        frame.setVisible(true);
    }

    public static void main(String[] args) {
        new FileBrowser().show();
    }
}